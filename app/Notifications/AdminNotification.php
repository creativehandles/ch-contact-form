<?php

namespace App\Notifications;

use Creativehandles\ChContactForm\ChContactFormFacade;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminNotification extends  BaseNotification implements ShouldQueue
{
    use Queueable;

    public $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->subject("Admin Notification")
            ->line(json_encode($this->data));

        return ChContactFormFacade::pushAttachments($mail,$this->data);
    }
}
