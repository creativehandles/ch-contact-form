<?php

namespace Creativehandles\ChContactForm\Tests;

use Creativehandles\ChContactForm\ChContactFormFacade;
use Orchestra\Testbench\TestCase;
use Creativehandles\ChContactForm\ChContactFormServiceProvider;
use Illuminate\Http\UploadedFile;

class Base extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [ChContactFormServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return ['ch-contact-form' => ChContactFormFacade::class];
    }

    protected function defineEnvironment($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'ch-contact-form');
        $app['config']->set('database.connections.ch-contact-form', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    public function formData()
    {
        return [
            "name"=> "User name",
            "surname"=> "User surname",
            "email"=> "user@email.com",
            "phone"=> "+885985887",
            "city"=> "User city",
            "g-recaptcha-response"=> "111",
            "form_name"=> "form_name",
            "cv1"=> UploadedFile::fake()->create('document1.pdf'),
            "cv2"=> UploadedFile::fake()->create('document2.pdf')
        ];
    }

    public function formConfig()
    {
        return config('ch-contact-form.form_name');
    }
}
