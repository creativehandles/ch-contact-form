<?php

use Creativehandles\ChContactForm\ChContactFormFacade;
use Creativehandles\ChContactForm\Tests\Base;

class ChContactFormTest extends Base
{
    public function test_save_file_attachments()
    {
        $this->assertNotEquals(
            $this->formData(),
            ChContactFormFacade::saveAttachments($this->formData(), $this->formConfig()['attachments'])
        );
    }

    public function test_get_notification_attachment_type()
    {
        $this->assertEquals('url', ChContactFormFacade::getNotificationAttachmentType([
            'type' => 'url',
            'link' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf'
        ]));

        $this->assertEquals('local', ChContactFormFacade::getNotificationAttachmentType([
            'type' => 'local',
            'field' => 'cv1',
            'disk' => 'local',
            'delete_on_success' => true,
            'link' => null
        ],));
    }
}
