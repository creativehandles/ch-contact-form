<?php

namespace Creativehandles\ChContactForm\Listeners;

use Creativehandles\ChContactForm\ChContactFormFacade;
use Illuminate\Support\Facades\Storage;

class DeleteChContactFormAttachmentsListener
{
    const ATTACHMENTS = 'notification_config.attachments';

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $notification_data = $event->notification->data;
        if (
            array_has($notification_data, self::ATTACHMENTS) &&
            !empty(array_get($notification_data, self::ATTACHMENTS)) &&
            $event->notification instanceof $notification_data['notification_config']['name']
        ) {
            foreach (array_get($notification_data, self::ATTACHMENTS) as $attachment) {
                if (
                    ChContactFormFacade::getNotificationAttachmentType($attachment) == 'local' &&
                    isset($notification_data[$attachment['field']]) &&
                    Storage::exists($notification_data[$attachment['field']], $attachment['disk']) &&
                    $attachment['delete_on_success']
                ) {
                    Storage::delete($notification_data[$attachment['field']], $attachment['disk']);
                }
            }
        }
    }
}
