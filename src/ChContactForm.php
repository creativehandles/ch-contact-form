<?php

namespace Creativehandles\ChContactForm;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class ChContactForm
{
    public $file_name, $file, $config;

    public function saveAttachments(array $form_data, array $form_attachment_config)
    {
        foreach ($form_attachment_config as $config) {
            $this->config = $config;

            if (isset($form_data[$this->config['field']]) && $form_data[$this->config['field']] instanceof UploadedFile) {
                $this->file = $form_data[$this->config['field']];
                $this->generateFileName();
                unset($form_data[$this->config['field']]);
                $form_data[$this->config['field']] = $this->store();
            }
        }
        return $form_data;
    }

    public function generateFileName(): void
    {
        switch ($this->config['save']['file']['name']) {

            case 'original':
                $this->file_name = $this->file->getClientOriginalName();
                break;

            case 'given':
                $this->file_name = ($this->config['save']['file']['given_name'] ?? config('ch-contact-form.default.file_name'));
                $this->file_name .= '.' . $this->file->extension();
                break;

            default:
                $this->file_name = null;
                break;
        }
    }

    public function store(): string
    {
        if ($this->file_name) {
            return $this->file->storeAs(
                $this->getStore('path'),
                $this->file_name,
                $this->getStore('disk')
            );
        }
        return $this->file->store(
            $this->getStore('path'),
            $this->getStore('disk')
        );
    }

    public function notify(array $data, array $form_config): bool
    {
        if (!(isset($form_config['notifications']) && count($form_config['notifications']) > 0)) return FALSE;

        foreach (Arr::get($form_config, 'notifications') as $notification_config) {
            $this->processNotification($data, $notification_config);
        }
        return TRUE;
    }

    public function processNotification($notification_data, $notification_config)
    {
        $class = $notification_config['name'];
        Notification::route(
            'mail',
            $this->getEmails($notification_data, $notification_config['notify'])
        )->notify((new $class(array_merge(
            $notification_data,
            [
                'notification_config' => $notification_config
            ]
        )))->locale(app()->getLocale()));
    }
    public function getEmails($data, $notify)
    {
        if (!$notify['form_input']) return explode(',', $notify['values']);
        return Arr::get($data, $notify['values']);
    }

    public function getStore(string $conf): string
    {
        switch ($conf) {
            case 'path':
                return Arr::get($this->config, 'save.in.path') ?? config('ch-contact-form.default.path');
                break;
            case 'disk':
                return Arr::get($this->config, 'save.in.disk') ?? config('ch-contact-form.default.storage');
                break;

            default:
                return null;
                break;
        }
    }

    public function getNotificationAttachmentType($attachment)
    {
        if (isset($attachment['type']) && !empty($attachment['type'])) {
            if ($attachment['type'] == 'url' && isset($attachment['link']) && !empty($attachment['link'])) {
                return 'url';
            } elseif (
                $attachment['type'] == 'local' &&
                isset($attachment['field']) &&
                !empty($attachment['field']) &&
                isset($attachment['disk']) &&
                !empty($attachment['disk'])
            ) {
                return 'local';
            }
        }
        return null;
    }

    public function pushAttachments($notification,$notification_data)
    {
        if (!isset($notification_data['notification_config']['attachments'])) return $notification;

        foreach ($notification_data['notification_config']['attachments'] as $attachment) {
            if ($this->getNotificationAttachmentType($attachment) == 'url') {
                $notification = $notification->attach($attachment['link']);
            } elseif ($this->getNotificationAttachmentType($attachment) == 'local') {
                if (
                    isset($notification_data[$attachment['field']]) &&
                    Storage::exists($notification_data[$attachment['field']], $attachment['disk'])
                ) {
                    $notification = $notification->attach(Storage::path($notification_data[$attachment['field']], $attachment['disk']));
                }
            }
        }
        return $notification;
    }
}
