<?php

namespace Creativehandles\ChContactForm\Providers;

use Creativehandles\ChContactForm\Listeners\DeleteChContactFormAttachmentsListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'Illuminate\Notifications\Events\NotificationSent' => [
            DeleteChContactFormAttachmentsListener::class
        ]
    ];

    public function boot()
    {
        parent::boot();
    }
}
