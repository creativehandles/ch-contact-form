<?php

namespace Creativehandles\ChContactForm;

use Creativehandles\ChContactForm\{
    Console\BuildChContactFormsPackageCommand,
    Providers\EventServiceProvider
};
use Illuminate\Support\ServiceProvider;

class ChContactFormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */

        if ($this->app->runningInConsole()) {

            //publish config
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('ch-contact-form.php'),
            ], 'config');

            // publish controllers
            $this->publishes([
                __DIR__ . '/../app/' => app_path('/'),
            ], 'publish app directory');

            //publishing routes
            $this->publishes([
                __DIR__ . '/../routes' => base_path('routes/packages'),
            ], 'routes');

            // Registering package commands.
            $this->commands([
                BuildChContactFormsPackageCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ch-contact-form');

        // Register the main class to use with the facade
        $this->app->singleton('ch-contact-form', function () {
            return new ChContactForm;
        });

        //Register Event Service Provider
        $this->app->register(EventServiceProvider::class);
    }
}
