<?php

namespace Creativehandles\ChContactForm;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChContactForm\Skeleton\SkeletonClass
 */
class ChContactFormFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-contact-form';
    }
}
