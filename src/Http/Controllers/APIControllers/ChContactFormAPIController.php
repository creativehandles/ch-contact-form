<?php

namespace Creativehandles\ChContactForm\Http\Controllers\APIControllers;

use Illuminate\Routing\Controller;
use App\Services\ExternalApiService;
use Creativehandles\ChContactForm\ChContactFormFacade;
use Creativehandles\ChContactForm\Requests\ChFormRequest;
use Illuminate\Http\Request;

class ChContactFormAPIController extends Controller
{
    /**
     * @var ExternalApiService
     */
    private $apiService;

    public function __construct(ExternalApiService $apiService, Request $request)
    {
        app()->setLocale($request->header('Content-locale', config('ch-contact-form.default.locale')));
        $this->apiService = $apiService;
    }

    public function submitForm(ChFormRequest $request)
    {
        $form_config = config('ch-contact-form.' . $request->form_name);
        try {

            //Save File content if exists
            $data = ChContactFormFacade::saveAttachments($request->validated(), array_get($form_config, 'attachments'));

            //send Notifications 
            ChContactFormFacade::notify($data, $form_config);

            return $this->apiService->success(array_get($form_config, 'message_on.success'));
        } catch (\Exception $exception) {
            logger()->error($exception);
            return $this->apiService->failed(
                array_get($form_config, 'message_on.fail'),
                500,
                $exception->getMessage() . ' on line '. $exception->getLine().' file '.$exception->getFile()
            );
        }
    }
}
