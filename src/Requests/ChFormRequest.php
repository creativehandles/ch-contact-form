<?php

namespace Creativehandles\ChContactForm\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use ReCaptcha\ReCaptcha;
use ReCaptcha\RequestMethod\CurlPost;

class ChFormRequest extends FormRequest
{
    public $form_config;

    public function __construct(Request $request)
    {
        $this->form_config = config('ch-contact-form.' . $request->form_name);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if (!array_get($this->form_config, 'recaptcha.enabled')) return true;

        $recaptcha = new ReCaptcha(config('services.google.recaptcha.secret_key'), new CurlPost());

        if (isset($this->form_config['recaptcha']['version']) && array_get($this->form_config, 'recaptcha.version') == 'v3') {
            $recaptcha->setExpectedAction(array_get($this->form_config, 'recaptcha.action'))
                ->setScoreThreshold(config('services.google.recaptcha.default_score_threshold'))
                ->setChallengeTimeout(config('services.google.recaptcha.default_challenge_timeout'));
        }

        $response = $recaptcha->verify($request[array_get($this->form_config, 'recaptcha.token_field')], $request->ip());
        return $response->isSuccess();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(array_get($this->form_config, 'param') ?? ['form_name' => 'required']);
    }
}
