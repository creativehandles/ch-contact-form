<?php

namespace Creativehandles\ChContactForm\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class BuildChContactFormsPackageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'creativehandles:build-ch-contact-forms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare Plugin Environment';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $plugin = 'ChForms';

        $this->warn('Publising vendor directories');
        $this->callSilent('vendor:publish', [
            '--force' => true,
            '--provider' => 'Creativehandles\ChContactForm\ChContactFormServiceProvider'
        ]);
        $this->info('Vendor Directories Published');

        $this->warn('Checking plugin already installed');
        if (!DB::table('active_plugins')->where('plugin', $plugin)->first()) {
            $this->info('Plugin not installed in system');
            $this->warn('Update plugin details on the system');
            DB::table('active_plugins')->insert([
                'plugin' => $plugin,
            ]);
            $this->info('Plugin detail updated on the system');
        }

        $this->warn('Linking system Storage');
        Artisan::call('storage:link');
        $this->info('System storage linked successful');

        $this->info('Good to go!!');
    }
}
