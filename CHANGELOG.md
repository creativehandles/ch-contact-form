# Changelog

All notable changes to `ch-contact-form` will be documented in this file

## 1.0.0 - 2021-10-27

- initial release
