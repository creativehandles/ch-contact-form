<?php
Route::group(['name' => 'ch-contact-forms'], function () {
    Route::post('/forms', 'APIControllers\PluginsControllers\ChContactFormAPIController@submitForm')->name('submit');
});