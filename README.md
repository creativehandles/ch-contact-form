# Creativehandles Dynamic Contact Forms Plugin

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/ch-contact-form.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-contact-form)

[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/ch-contact-form.svg?style=flat-square)](https://packagist.org/packages/creativehandles/ch-contact-form)

![GitHub Actions](https://github.com/creativehandles/ch-contact-form/actions/workflows/main.yml/badge.svg)

This package designed for manipulate multiple forms with simple configurations.This package includes Notification with Attachments,File upload,Form Validation,

## Installation

You can install the package via composer:

```bash
composer require creativehandles/ch-contact-form
```

Then build plugin environment and publish its resources

```bash
php artisan creativehandles:build-ch-contact-forms
```

## Usage

After installation, you can see the default **ch-contact-form.php** config file under the config directory. This is the most important part of this package. By Editing this file you can create multiple contact forms much as you want.

---

### Basic config file structure

Here is the basic structure of the config file.This file mainly contains two important part

> 1.Default Configurations

> 2.Form Blocks

```php
[

/** Default Values **/
	'default' => [
		'storage' => 'local',
		'path' => 'temp',
		'file_name' => md5(uniqid(time())),
		'locale' => 'cs',  // Set `Content-locale` header to override this value
	],
/** END Default Values **/

/** START Form BLOCK Configuration */
'YOUR_FORM_NAME' => [ 			// 1
	'recaptcha' => [..],		// 2
	'param' => [..],			// 3
	'attachments' => [..],		// 4
	'message_on' => [..],		// 5
	'notifications' => [..],	// 6
];
/** END Form BLOCK Configuration*/
```

# FormBlock Configuraion

_NOTE - default values applied when related values are not presented_

1. YOUR_FORM_NAME
   
   This is your form name and it should be unique value.When you are calling this form via API you need to pass this key name under request `form_name` parameter the plugin pick related configurations.

```php
/** START Form BLOCK Configuration */
'YOUR_FORM_NAME' => [
		//other configurations
];
/** END Form BLOCK Configuration*/
```

2. Recaptcha
   
   This configuration allows you to have google recaptcha `V2`,`V3` validation into your form. First of all you need to enabled google recaptcha inside your app by providing below configuration inside your `services.php` configuration file.
   *NOTE : You need to install `"google/recaptcha": "^1.2",` library in your app in order to work recaptcha*
   
   ```bash
   composer require google/recaptcha": "^1.2
   ```

_config/services.php_

```php
'google' => [
	'recaptcha' => [
		'site_key' => env('GOOGLE_RECAPTCHA_SITE_KEY'),
		'secret_key' => env('GOOGLE_RECAPTCHA_SECRET_KEY'),
		'default_score_threshold' =>env('GOOGLE_RECAPTCHA_DEFAULT_SCORE_THRESHOLD'),
		'default_challenge_timeout' => env('GOOGLE_RECAPTCHA_DEFAULT_CHALLENGE_TIMEOUT')
	]
],
```

By setting `enabled` to `TRUE` recaptcha can be enabled inside your form. Then you need to add the following values to work it properly otherwise recaptcha will not work. `action` value which is you provide to the recaptcha `JS` library. After doing a successful API call the google recaptcha will return _recaptcha token_. Then you need to pass that token to the plugin to validate the form with recaptcha. Therefore backend need to identify token field name which is included form request itself.You can define that field by providing that field name under the `token_field` key.

By default recaptcha enables for version `V2`. if you need to use version `V3`, Provide `v3` under version key.

```php
'recaptcha' =>[
	'enabled' => false,
	'action' => 'ACTION',
	'token_field' => 'TOKEN_FIELD_NAME',
	'version'=> null,  //v2 or v3 
]
```

3. Param

   Under this key, You can define your form field names with their backend validation rules. you can use the field name as a `key` and backend validation as a `value`

```php
'param' => [
	//'field_name' => 'FIELD_VALIDATION_RULES'
	'name' => 'required|string',
	'file' => 'sometimes|nullable|file|mimes:docx,doc,odt,pdf,jpeg,png',
],
```

4. Attachments
   
   If your form contains file uploads you can choose how to save that.

```php
'attachments' => [

	/** START Attachment block */
	[
		'field' => 'FILE_UPLOAD_FIELD_NAME',
		'save' => [
			'file' => [
				'name' => 'original',
				'given_name' => null
			],
			'in' => [
				'path' => null,
				'disk' => null
			],
	]
	/** END Attachment Block */
]
```

Therefore you need to pass the request file upload form field exact name under the `field` key. To determine how to save that file in your storage you can use `save` array key. `name` key which is located under `field` array key can have three main value **random** | **given** | **original** its functions as follows

- random - this will generate random file names to your file on save
- original - this will take uploaded file name as a save filename
- give - if you choose this you can provide your desired name as a save file name under the `given_name` key. If not default file name will be applied to the file generated under the `default->file_name` key

```php
'file' => [
	'name' => 'original',
	'given_name' => null
],
```

To determine where to save the file by using `in` array key. You can specify the save path and the disk. Default values will be applied if array key values are not presented

```php
'in' => [
	'path' => null,
	'disk' => null
],
```

5. Message On

   These values will be returned when form action Success or Fails

```php
'message_on' => [
	'success' => 'Form Submission Successful',
	'fail' => 'Form Submission Unsuccessful',
],
```

6. Notifications

You can also send notifications with this plugin. Therefore you need to add notification configuration blocks.

```php
'notifications' => [

	/** START Notification Block */
	[
		'name' => 'YOUR_NOTIFICATION_NAME_SPACE',
		'notify' => [
			'form_input' => true,
			'values' => 'email'
		],
		'attachments' => [
			[
				'type' => 'url',
				'link' => 'FILE_URL'
			],
			[
				'type' => 'local',
				'field' => 'FILE_NAME_FIELD_ON_REQUEST',
				'disk' => 'FILE_SAVE_DISK',
				'delete_on_success' => true,
				'link' => null
			],
		]
	]
	/** END Notification Block */
]
```

You can use your own notification class, Therefore you need to pass the notification class namespace under `name` key.then you can choose who needs to get notified by configuring `notify` key

```php
'notify' => [
	'form_input' => true,
	'values' => 'email'
],
```

if you use form filed value as a notifier you need to set `form_input` key as a `TRUE` then under the`values` key you need to pass that form field name as a value. Or else you can set `form_input` as a `FALSE`
and give some emails by separating using comma( `,`)

if you are planning to send some attachments via notification. You can do that by using the `attachments` object located inside the `notification` object. Also need to call the following method inside the notification notify method in order to add that attachment to the notification. By default, the plugin injects form data into the notification construct method.

```php
\Creativehandles\ChContactForm\ChContactFormFacade::pushAttachments($notify,$notification_data)
```

below example show mail notification example.

```php
/** This is a Notification Class */
public  $data; // form data injected by plugin

public  function  __construct(array  $data)
{
	$this->data = $data;
}

public  function  toMail($notifiable)
{
	$mail = (new  MailMessage)
		->subject('Something')
		->line('hello');
	return  \Creativehandles\ChContactForm\ChContactFormFacade::pushAttachments($mail,$this->data);
}
```

```php
'attachments' => [
	[
		'type' => 'url',
		'link' => 'FILE_URL'
	],
	[
		'type' => 'local',
		'field' => 'FILE_NAME_FIELD_ON_REQUEST',
		'disk' => 'FILE_SAVE_DISK',
		'delete_on_success' => true
	],
]
```

You can attach `form input file ` or `External file from URL`

- To attach form request input file. Do following configuration
  Set `type` as a `local` then provide form field name under the `field` key. Then file save disk under the `disk` key. If you wish to delete file attachment from your storage on the notification sent then set `delete_on_success` to `TRUE`.

- To attach external file from external url
  Set `type` as a `url`. finally place url under the `link` key

---

### How to make api call for created form

The plugin has provided an API endpoint to call your form. To identify your form you need to pass your form name under the `form_name` request parameter along with the request

Endpoint - `BASE_URL/api/v1/forms`
Method - `POST`
Header - `Content-locale:YOUR_LOCAL` //header not presented default locale will be applied from plugin config
Form param -

```
{
	"form_name": "YOUR_FORM_NAME", //must include this
	......other form fields.......
}
```
**ERROR STATUS CODES**
- 403 - Invalid Recaptcha
- 200 - Form Submission Successful
- 422 - Validation Error
- 500 - Internal Server Error  

_NOTE - By providing Content locale header you can pass localize notification_

---

Full Configuration File

```php
<?php

return [
	/** Default Values **/
	'default' => [
		'storage' => 'local',
		'path' => 'temp',
		'file_name' => md5(uniqid(time())),
		'locale' => 'cs'  // Set `Content-locale` header to override this value
	],
	/** END Default Values **/

	/** START Form BLOCK Configuration */
	'YOUR_FORM_NAME' => [
		'recaptcha' =>[
			'enabled' => false,
			'action' => 'ACTION',
			'token_field' => 'TOKEN_FIELD_NAME',
			'version'=> null,  //v2 or v3 | By default v2
		],
		'param' => [
			//'field_name' => 'FIELD_VALIDATION_RULES'
			'name' => 'required|string',
		],
		'attachments' => [
			/** START Attachment block */
			[
			'field' => 'FILE_UPLOAD_FIELD_NAME',
			'save' => [
				'file' => [
					'name' => 'original', // random|original|given
					'given_name' => null
				],
				'in' => [
					'path' => null,
					'disk' => null
				],
			]
			/** END Attachment Block */
		],
		'message_on' => [
			'success' => 'Form Submission Successful',
			'fail' => 'Form Submission Unsuccessful',
		],
		'notifications' => [
			/** START Notification Block */
			[
				'name' => 'YOUR_NOTIFICATION_NAMESPACE',
				'notify' => [
					'form_input' => true,
					'values' => 'email'
				],
				'attachments' => [
					[
						'type' => 'url',
						'link' => 'FILE_URL'
					],
					[
						'type' => 'local',
						'field' => 'FILE_NAME_FIELD_ON_REQUEST',
						'disk' => 'FILE_SAVE_DISK',
						'delete_on_success' => true,
						'link' => null
					],
				]
			]
			/** END Notification Block */
		],
	]
		/** END Form BLOCK Configuration*/
]
```

---

### Testing

```bash

composer test

```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email kasun.eranda@creativehandles.com instead of using the issue tracker.

## Credits

- [Kasun Eranda](https://github.com/creativehandles)

- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
