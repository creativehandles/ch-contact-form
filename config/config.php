<?php

/*
 * You can place your custom package configuration in here.
 */

return [
    'default' => [
        'storage' => 'local',
        'path' => 'temp',
        'file_name' => md5(uniqid(time())),
        'locale' => 'cs' // Set `Content-locale` header to override this value
    ],
    'form_name' => [
        'recaptcha' => [
            'enabled' => false,
            'action' => 'ACTION',
            'token_field' => 'TOKEN_FIELD_NAME',
            'version'=> null,  //v2 or v3 | By default v2
        ],
        'param' => [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string',
            'city' => 'required|string',
            'cv1' => 'sometimes|nullable|file|mimes:docx,doc,odt,pdf,jpeg,png',
            'cv2' => 'sometimes|nullable|file|mimes:docx,doc,odt,pdf,jpeg,png',
            'message' => 'sometimes|nullable|string',
        ],
        'attachments' => [
            [
                'field' => 'cv1',
                'save' => [
                    'file' => [
                        'name' => 'original', // random | given | original
                        'given_name' => null // file_name only consider when "name" key equal to given. Also default file name will be apply when the "given_name" key is null and "name" key equal to `given`
                    ],
                    'in' => [
                        'path' => null,  //default storage path will be apply when the "path" key is null 
                        'disk' => null   //default storage will be apply when the "disk" key is null
                    ],
                ],
            ],
            [
                'field' => 'cv2',
                'save' => [
                    'file' => [
                        'name' => 'original', // random | given | original
                        'given_name' => null // file_name only consider when "name" key equal to given. Also default file name will be apply when the "given_name" key is null and "name" key equal to `given`
                    ],
                    'in' => [
                        'path' => null,  //default storage path will be apply when the "path" key is null 
                        'disk' => null   //default storage will be apply when the "disk" key is null
                    ],
                ],
            ]
        ],
        'message_on' => [
            'success' => 'Form Submission Successful',
            'fail' => 'Form Submission Unsuccessful',
        ],
        'notifications' => [
            [
                'name' => '\App\Notifications\CustomerNotification',
                'notify' => [
                    'form_input' => true,
                    'values' => 'email'
                ],
                'attachments' => [
                    [
                        'type' => 'url',
                        'link' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf'
                    ]
                ]
            ],
            [
                'name' => '\App\Notifications\AdminNotification',
                'notify' => [
                    'form_input' => true,
                    'values' => 'email'
                ],
                'attachments' => [
                    [
                        'type' => 'local',
                        'field' => 'cv1',
                        'disk' => 'local',
                        'delete_on_success' => true
                    ],
                    [
                        'type' => 'local',
                        'field' => 'cv2',
                        'disk' => 'local',
                        'delete_on_success' => false
                    ]
                ]
            ]
        ]

    ],

];
